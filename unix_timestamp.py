#!/usr/bin/python
import time
import datetime
import re
n = datetime.datetime.now()
inpu = raw_input('enter the input')
#print inpu
regex = re.compile(r'(\d+.*),.*overflow:(\d+).*logline_in:(\d+).*logline_out:(\d+)', flags=re.IGNORECASE)
date = regex.match(inpu)
#print date.group(0)
#print "date is",re.sub("[^\d:]","/",date.group(1))
logline_in = date.group(3)
logline_out = date.group(4)
overflow = date.group(2)
dt = datetime.datetime.strptime(re.sub("[^\d:]","/",date.group(1)),"%Y//%m//%d/%H:%M:%S")
#print dt.timetuple()
unix_time =  time.mktime(dt.timetuple())
print "############# The Result is ################"
print "%s:%s:%s:%s" %(unix_time,logline_in,logline_out,overflow)
