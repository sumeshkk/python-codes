#!/usr/bin/python
import os
from os.path import expanduser

file = expanduser("~") + "/perl5/perlbrew/bin/perlbrew"
cpanm_root = expanduser("~") + "/perl5/perlbrew/bin/cpanm"

if(not os.path.isfile(file) or not os.path.isfile(cpanm_root) ):
 print "not exist"
