#!/bin/bash

## These 3 lines are mandatory.
export PERLBREW_ROOT=${HOME}/perl5/perlbrew
export PERLBREW_HOME=/tmp/.perlbrew
echo ${PERLBREW_ROOT}
source ${PERLBREW_ROOT}/etc/bashrc
