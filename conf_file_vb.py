#!/usr/bin/python

import sys

def write(filename,content):
    print('Creating new configuration file') 
    
    if filename:
        name = filename+'.conf'  # Name of text file coerced with +.conf
    else:
	print "\n Please provid file name for function write()"
    try:
        file = open(name,'w')   # Trying to create a new file or open one
	if content and type(content) is not str:
	    for line in content:
	        file.write(line+'\n') 
            file.close()
	else:
	     file.write(content)
	     file.close()
    except:
        print('Something went wrong! Can\'t tell what?')
        sys.exit(0) # quit Python

conf_content = '''
LoadModule env_module         /usr/lib/apache2/modules/mod_env.so
LoadModule mime_module        /usr/lib/apache2/modules/mod_mime.so
LoadModule negotiation_module /usr/lib/apache2/modules/mod_negotiation.so
LoadModule autoindex_module   /usr/lib/apache2/modules/mod_autoindex.so
LoadModule dir_module         /usr/lib/apache2/modules/mod_dir.so
LoadModule alias_module       /usr/lib/apache2/modules/mod_alias.so
LoadModule authz_host_module  /usr/lib/apache2/modules/mod_authz_host.so 
LoadModule authn_file_module /usr/lib/apache2/modules/mod_authn_file.so
LoadModule setenvif_module    /usr/lib/apache2/modules/mod_setenvif.so
LoadModule access_compat_module /usr/lib/apache2/modules/mod_access_compat.so
LoadModule mpm_prefork_module /usr/lib/apache2/modules/mod_mpm_prefork.so
LoadModule perl_module /usr/lib/apache2/modules/mod_perl.so
LoadModule authz_core_module /usr/lib/apache2/modules/mod_authz_core.so
LoadModule authz_groupfile_module /usr/lib/apache2/modules/mod_authz_groupfile.so
LoadModule apreq_module /usr/lib/apache2/modules/mod_apreq2.so
LoadModule headers_module  /usr/lib/apache2/modules/mod_headers.so
'''
write('perl.vbarter.com',conf_content)
