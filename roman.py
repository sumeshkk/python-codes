#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Purpose: Convert Roman letters in given file to minimal form and write it to anothe fiel 
#	  named new_roman.txt
# Author : Sumesh.k.k
# Date   : 21-feb-2013

import sys


roman_values = (('I',1), ('IV',4), ('V',5), ('IX',9),('X',10),('XL',40),('L',50),('XC',90),('C',100),('CD', 400), ('D', 500), ('CM', 900), ('M',1000))

#Function to find the tottal value
def romanvalue(roman_number):	
    roman_number_total=0
    roman_number_copy = roman_number[:].upper()	
    for roman_symbol,roman_symbol_value in reversed(roman_values):
        while roman_number_copy.startswith(roman_symbol):
            roman_number_total += roman_symbol_value
            roman_number_copy = roman_number_copy[len(roman_symbol):]    
    return roman_number_total

#Function to find the minimal representation
def minimal(total):							
    value = ''
    while total > 0:
        for symbol,symbol_value in reversed(roman_values):
            while total >= symbol_value:
                value += symbol
                total -= symbol_value
    return value

#Execution starts from here
if len(sys.argv)>1:
     file_name = sys.argv[1]
     FH = open (file_name,"r")
     FH2= open("./new_roman.txt","w")                   #file to read
     FH2.write("Modified Document\n")			#file to write
     for line in FH:
        totalvalue   = romanvalue(line)
        minimalrepr = minimal(totalvalue)
        FH2.write(("Given : %-15s\tMinimal : %-15s\tValue : %i\n" %(line[:-1],minimalrepr,totalvalue)))
      	print ('Minimal representation is: %s (value is : %i)' %(minimalrepr,totalvalue))
else:
	print 'Enter the file name as argument (eg: roman.py filename)'

