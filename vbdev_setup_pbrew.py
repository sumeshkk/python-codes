#!/usr/bin/python

# Author : Sumesh k k
# Date : 29-Sep-2014
# Purpose : Setup an environment for vb server

# Import the module
import subprocess
import fileinput
import sys
import os
import re
import pexpect
import time
from os.path import expanduser
# Ask the user for input
#host = raw_input("Enter a host to ping: ")	
command = "sudo apt-get -y install apache2 apache2-utils libapache2-mod-auth-pgsql libapache2-mod-perl2 libapache2-mod-perl2-dev libapache2-mod-php5 libapache2-mod-python libapache2-mod-wsgi libapache2-mod-fastcgi libapache2-mod-apreq2 libapache2-request-perl libdatetime-perl libcurl4-gnutls-dev librtmp-dev libxml-libxml-perl libdebug-trace-perl libdebug0-dev libdebug-client-perl libxml-libxslt-perl libgd-gd2-perl perlmagick postgresql postgresql-contrib libpq-dev curl"

new_snap_shot_file = 'SnapshotVB.pm'

########  Definitions ##########

def runcommand(command):
  # Set up the echo command and direct the output to a pipe
  p1 = subprocess.Popen(command.split())
  retval = p1.wait()
  return retval

def snapshot_file_alt(file):
 if os.path.isfile(file) and os.access(file, os.R_OK):
  snap_newfile_ = open(expanduser("~")+"/.cpan/Bundle/" + new_snap_shot_file ,'w')
  for line in open(file):
   match = re.search("^([\w\:]+) [\d\.]+$",line)       
   if match:                                            # if the line contains module information
     match_modperl = re.search("^mod_perl.*",line)      
     if match_modperl:					# if the line contains mod_perl info write with blank
       snap_newfile_.write(" ")
     else:
       snap_newfile_.write(match.group(1)+" undef")     # remove version info
   else:
    snap_newfile_.write(line)				# if the line is not about perl modules
 else:
  return 0               # Can't open file or does not have permission
 return 1		 # File read and write successfully


def relaxing(withstring):
 count = 10
 while(count >= 0):
  print withstring * count
  time.sleep(0.50)
  count = count - 1


def print_string(string):
 print "\n############################ " + string + " ############################\n"



######## Definitions Ends here ######

  
### Os Package installation
print_string("PACKAGES INSTALLATION STARTS HERE")
#sucess = runcommand(command)
#print sucess
print_string("PACKAGES INSTALLATION ENDS HERE")

relaxing("*")

# Perlbrew installation

#os.system('curl -L http://install.perlbrew.pl | bash')
profile_file_sh = open(expanduser("~")+"/.bash_profile",'w')
profile_file_sh.write("source " + expanduser("~") + "/perl5/perlbrew/etc/bashrc" )
profile_file_sh.close

# Perlbrew enviroment starting 
#source = raw_input("\nGive the source that printed above from perlbrew >")
'''
runcommand("source " + expanduser("~")+"/.bash_profile")
runcommand("perlbrew install-cpanm")
runcommand("cpanm Bundle::CPAN")
'''
# From server take the snapshot
# perl -MCPAN -e 'autobundle'

cpan_snap_file = raw_input("\nEnter Autobundle file full path :>")   

## Modifying the autobundle file to remove versions, which will install latest modules
'''
if(cpan_snap_file):
 if(snapshot_file_alt(cpan_snap_file)):
  print "\nSuccessfully replaced all versions with 'undef' in file ~/.cpan/Bundle/" + new_snap_shot_file
 else: 
  print cpan_snap_file + "doesnot exist or not dont have permission to open"
else:
 print "\nNo Autobundle file provided"
'''
## Installing module one by one
sp = subprocess.Popen(["/bin/bash", "-i", "-c", "nuke -x " + expanduser("~") + "/.bash_profile"])
sp.communicate()

new_snap_file = expanduser("~")+"/.cpan/Bundle/" + new_snap_shot_file
if(os.path.isfile(new_snap_file)):
 for line in open(new_snap_file):
  match = re.search("^([\w\:]+) [\w]+$",line)       
  if match:     
   runcommand("cpanm install " + match.group(1) )     
else:
 print "\nNo Snapshot file in location -: " + expanduser("~")+"/.cpan/Bundle/" + new_snap_shot_file



